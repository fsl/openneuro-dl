from setuptools import setup,find_packages
with open('requirements.txt', 'rt') as f:
    install_requires = [l.strip() for l in f.readlines()]

setup(name='get_dataset',
	version='1.0.0',
	description='FSL dataset downloader. Useful for downloading open neuroimaging datasets from various online repos',
	author='Taylor Hanayik',
	install_requires=install_requires,
    scripts=['get_dataset/get_dataset'],
	packages=find_packages(),
	include_package_data=True)

